let conexion = require('./bdconfig');

module.exports = class Partido {
    constructor(partidoJSON) {
        this.COD_ID = partidoJSON.COD_ID;
        this.EQUIPO_L = partidoJSON.EQUIPO_L;
        this.EQUIPO_V = partidoJSON.EQUIPO_V;
        this.FECHA = partidoJSON.FECHA;
        this.SEDE = partidoJSON.SEDE;
        this.RESULTADO_L = partidoJSON.RESULTADO_L;
        this.RESULTADO_V = partidoJSON.RESULTADO_V;
        this.ASISTENCIA = partidoJSON.ASISTENCIA;
        this.SOLICITANTE = partidoJSON.SOLICITANTE;
        this.ACCESO = partidoJSON.ACCESO;
    }

    static listarPartidos(usuario, acceso) {
        return new Promise((resolve, reject) =>{
        if (acceso == 'BASIC'){
            conexion.query("SELECT e.EQUIPO, COUNT(*), (SELECT SUM(p.RESULTADO_L) FROM PARTIDO p WHERE p.EQUIPO_L=e.EQUIPO) AS GOLES_LOCAL, (SELECT SUM(p.RESULTADO_V) FROM PARTIDO p WHERE p.EQUIPO_V=e.EQUIPO) AS GOLES_VISITANTE FROM PARTIDO p, EQUIPOS e WHERE (e.EQUIPO = p.EQUIPO_L OR e.EQUIPO = p.EQUIPO_V) AND e.EQUIPO = ? GROUP BY e.EQUIPO ",
            [usuario], (error, resultado, campos) =>{
                if(error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0])
            })
        } else {
            conexion.query("SELECT * FROM PARTIDO ORDER BY FECHA DESC;",
            (error, resultado, campos) =>{
                if(error || resultado.length == 0) return reject(error);
                else return resolve(resultado.map(partido => new Partido(partido)))
            })
            }
        });
    }

    static buscarPartidoPorId(id, usuario, acceso) {
        return new Promise((resolve, reject) =>{
            if (acceso == 'BASIC'){
                conexion.query("(SELECT * FROM partido WHERE EQUIPO_V = ? AND PARTIDO.COD_ID = ? ) UNION (SELECT * FROM partido WHERE EQUIPO_L = ? AND PARTIDO.COD_ID = ? )",
                [usuario, id, usuario, id], (error, resultado, campos) =>{
                    if(error || resultado.length == 0) return reject(error);
                    else return resolve(new Partido(resultado[0]))
                })
            } else {
                conexion.query("SELECT PARTIDO.* FROM PARTIDO WHERE PARTIDO.COD_ID = ?",
                [id], (error, resultado, campos) =>{
                    if(error || resultado.length == 0) return reject(error);
                    else return resolve(new Partido(resultado[0]))
                })
            }
        })
    }

    static listarPartidosImportantes(usuario, acceso) {
        return new Promise((resolve, reject) =>{
            if (acceso == 'BASIC'){
                conexion.query("(SELECT PARTIDO.* FROM PARTIDO WHERE EQUIPO_L LIKE ? AND MONTH(PARTIDO.FECHA) LIKE 7) UNION (SELECT PARTIDO.* FROM PARTIDO WHERE EQUIPO_V LIKE ? AND MONTH(PARTIDO.FECHA) LIKE 7 )",
                [usuario, usuario], (error, resultado, campos) =>{
                    if(error || resultado.length == 0) return reject(error);
                    else return resolve(resultado.map(partido => new Partido(partido)));
                })
            } else {
                conexion.query("SELECT PARTIDO.* FROM PARTIDO WHERE MONTH(PARTIDO.FECHA) LIKE 7 ",
                (error, resultado, campos) =>{
                    if(error || resultado.length == 0) return reject(error);
                    else return resolve(resultado.map(partido => new Partido(partido)))
                })
            }
        })
    }

    static listarPartidosSeleccion(usuario, acceso, equipo) {
        return new Promise((resolve, reject) =>{
            if (acceso == 'BASIC'){
                conexion.query("(SELECT * FROM partido WHERE EQUIPO_L = ? AND EQUIPO_V = ?) UNION (SELECT * FROM partido WHERE EQUIPO_L = ? AND EQUIPO_V = ?)",
                [usuario, equipo, equipo, usuario], (error, resultado, campos) =>{
                    if(error || resultado.length == 0) return reject(error);
                    else return resolve(resultado.map(partido => new Partido(partido)));
                })
            } else {
                conexion.query("SELECT * FROM partido WHERE EQUIPO_L = ? OR EQUIPO_V = ? ",
                [equipo, equipo], (error, resultado, campos) =>{
                    if(error || resultado.length == 0) return reject(error);
                    else return resolve(resultado.map(partido => new Partido(partido)))
                })
            }
        })
    }

    static listarPartidosSeleccionGoles(usuario, acceso, equipo, goles) {
        return new Promise((resolve, reject) =>{
            if (acceso == 'BASIC'){
                conexion.query("(SELECT * FROM partido WHERE EQUIPO_L = ? AND EQUIPO_V = ? AND RESULTADO_L+RESULTADO_V >= ? ) UNION (SELECT * FROM partido WHERE EQUIPO_L = ? AND EQUIPO_V = ? AND RESULTADO_L+RESULTADO_V >= ? )",
                [equipo, usuario, goles, usuario, equipo, goles], (error, resultado, campos) =>{
                    if(error || resultado.length == 0) return reject(error);
                    else return resolve(resultado.map(partido => new Partido(partido)));
                })
            } else {
                conexion.query("(SELECT * FROM partido WHERE (EQUIPO_L = ? OR EQUIPO_V = ? ) AND RESULTADO_L+RESULTADO_V >= ? )",
                [equipo, equipo, goles], (error, resultado, campos) =>{
                    if(error || resultado.length == 0) return reject(error);
                    else return resolve(resultado.map(partido => new Partido(partido)))
                })
            }
        })
    }

    crear() {
        return new Promise((resolve, reject) =>{
            if (this.ACCESO == 'BASIC'){
                if(this.SOLICITANTE === this.EQUIPO_L || this.SOLICITANTE === this.EQUIPO_V) { 
                    let datos = {EQUIPO_L: this.EQUIPO_L, EQUIPO_V: this.EQUIPO_V, FECHA: this.FECHA, SEDE: this.SEDE, RESULTADO_L: this.RESULTADO_L, RESULTADO_V: this.RESULTADO_V, ASISTENCIA: this.ASISTENCIA};
                    conexion.query("INSERT INTO PARTIDO SET ? ",
                    datos, (error, resultado, campos) =>{
                        if(error || resultado.affectedRows == 0) return reject(error);
                        else return resolve(resultado);
                    })
                }
            } else {
                let datos = {EQUIPO_L: this.EQUIPO_L, EQUIPO_V: this.EQUIPO_V, FECHA: this.FECHA, SEDE: this.SEDE, RESULTADO_L: this.RESULTADO_L, RESULTADO_V: this.RESULTADO_V, ASISTENCIA: this.ASISTENCIA};
                conexion.query("INSERT INTO PARTIDO SET ? ",
                datos, (error, resultado, campos) =>{
                    if(error || resultado.affectedRows == 0) return reject(error);
                    else return resolve(resultado);
                })
            }
        })
    }

    actualizar() {
        return new Promise((resolve, reject) =>{
            if (this.ACCESO == 'BASIC'){
                if(this.SOLICITANTE === this.EQUIPO_L || this.SOLICITANTE === this.EQUIPO_V) { 
                    let datos = {EQUIPO_L: this.EQUIPO_L, EQUIPO_V: this.EQUIPO_V, FECHA: this.FECHA, SEDE: this.SEDE, RESULTADO_L: this.RESULTADO_L, RESULTADO_V: this.RESULTADO_V, ASISTENCIA: this.ASISTENCIA};
                    conexion.query("UPDATE PARTIDO SET ? WHERE COD_ID = ?",
                    [datos, this.COD_ID], (error, resultado, campos) =>{
                        if(error || resultado.affectedRows == 0) return reject(error);
                        else return resolve(resultado);
                    })
                }
            } else {
                let datos = {EQUIPO_L: this.EQUIPO_L, EQUIPO_V: this.EQUIPO_V, FECHA: this.FECHA, SEDE: this.SEDE, RESULTADO_L: this.RESULTADO_L, RESULTADO_V: this.RESULTADO_V, ASISTENCIA: this.ASISTENCIA};
                conexion.query("UPDATE PARTIDO SET ? WHERE COD_ID = ?",
                [datos, this.COD_ID], (error, resultado, campos) =>{
                    if(error || resultado.affectedRows == 0) return reject(error);
                    else return resolve(resultado);
                })
            }
        })
    }

    borrar() {
        return new Promise((resolve, reject) =>{
            if (this.ACCESO == 'BASIC'){
                if(this.SOLICITANTE === this.EQUIPO_L || this.SOLICITANTE === this.EQUIPO_V) { 
                    conexion.query("DELETE FROM PARTIDO WHERE COD_ID = ?",
                    [this.COD_ID], (error, resultado, campos) =>{
                        if(error || resultado.affectedRows == 0) return reject(error);
                        else return resolve(resultado);
                    })
                }
            } else {
                conexion.query("DELETE FROM PARTIDO WHERE COD_ID = ?",
                [this.COD_ID], (error, resultado, campos) =>{
                    if(error || resultado.affectedRows == 0) return reject(error);
                    else return resolve(resultado);
                })
            }
        })
    }
}