const conexion = require('./bdconfig');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
const secreto = 'partidosNode';

module.exports = class Usuario {
    static validarUsuario(usuarioJSON) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT EQUIPO, ACCESO FROM EQUIPOS WHERE EQUIPO = ? AND PASSWORD = ?",
            [usuarioJSON.login, md5(usuarioJSON.password)],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(this.generarToken(resultado[0].EQUIPO, resultado[0].ACCESO));
            })
        })
    }

    static generarToken(login, acceso) {
        let token = jwt.sign({ login: login, acceso: acceso }, secreto, { expiresIn: "30 minutes" });
        return token;
    }

    static validarToken(token) {
        try {
            let resultado = jwt.verify(token, secreto);
            return resultado;
          } catch (e) {}
    }
}