const http = require('http');
const Usuario = require('./modelo/seleccion');
const Partido = require('./modelo/partido');

let atenderPeticion = (request, response) =>{
    if(request.method === 'GET'){
        if(request.url === '/Partidos') {
            let respuesta = {};
            let tokenRecibido = request.headers["authorization"];
            let token = Usuario.validarToken(tokenRecibido);
            if (tokenRecibido && token) {
                console.log(token.login);
                console.log(token.acceso);
                let nuevoToken = Usuario.generarToken(token.login, token.acceso);
                Partido.listarPartidos(token.login, token.acceso).then(resp =>{
                    response.writeHead(200, { "Content-Type": "application/json" });
                    respuesta.resultado = resp;
                    respuesta.token = nuevoToken;
                    response.end(JSON.stringify(respuesta));
                }).catch(error =>{
                    response.writeHead(400, { "Content-Type": "application/json" });
                    respuesta.error = true;
                    respuesta.mensajeError = 'Error al procesar la petición o no se han seleccionado partidos: ' + error;
                    response.end(JSON.stringify(respuesta))
                });
            } else {
                response.writeHead(401, { "Content-Type": "application/json" });
                respuesta.error = true;
                respuesta.mensajeError = 'Usuario no autorizado ';
                response.end(JSON.stringify(respuesta));
            }
        } else if(request.url === '/Partidos_importantes') {
            let respuesta = {};
            let tokenRecibido = request.headers["authorization"];
            let token = Usuario.validarToken(tokenRecibido);
            if(tokenRecibido && token) {
                let nuevoToken = Usuario.generarToken(token.login, token.acceso);
                Partido.listarPartidosImportantes().then(resp =>{
                    resp.SOLICITANTE = token.login;
                    resp.ACCESO = token.acceso;
                    respuesta.token = nuevoToken;
                    respuesta.error = false;
                    respuesta.mensajeError = '';
                    respuesta.partido = resp;
                    response.writeHead(200, { "Content-Type": "application/json" });
                    response.end(JSON.stringify(respuesta));
                }).catch(error =>{
                    response.writeHead(400, { "Content-Type": "application/json" });
                    respuesta.error = true;
                    respuesta.mensajeError = 'Error al procesar la petición o no se han seleccionado partidos: ' + error;
                    response.end(JSON.stringify(respuesta))
                })
            } else {
                response.writeHead(401, { "Content-Type": "application/json" });
                respuesta.error = true;
                respuesta.mensajeError = 'Usuario no autorizado ';
                response.end(JSON.stringify(respuesta));
            }
        } else if(request.url.startsWith('/Partidos_equipo/')) {
            let respuesta = {};
            let tokenRecibido = request.headers["authorization"];
            let token = Usuario.validarToken(tokenRecibido);
            if(tokenRecibido && token) {
                let nuevoToken = Usuario.generarToken(token.login, token.acceso);
                let parte = request.url.split('/');
                Partido.listarPartidosSeleccion(token.login, token.acceso, parte[2]).then(resp =>{
                    resp.forEach(ele => {
                        ele.SOLICITANTE = token.login;
                        ele.ACCESO = token.acceso;
                    });
                    respuesta.token = nuevoToken;
                    respuesta.error = false;
                    respuesta.mensajeError = '';
                    respuesta.partido = resp;
                    response.writeHead(200, { "Content-Type": "application/json" });
                    response.end(JSON.stringify(respuesta));
                }).catch(error =>{
                    response.writeHead(400, { "Content-Type": "application/json" });
                    respuesta.error = true;
                    respuesta.mensajeError = 'Error al procesar la petición o no se han seleccionado partidos: ' + error;
                    response.end(JSON.stringify(respuesta));
                });
            } else {
                response.writeHead(401, { "Content-Type": "application/json" });
                respuesta.error = true;
                respuesta.mensajeError = 'Usuario no autorizado ';
                response.end(JSON.stringify(respuesta));
            }
        } else if(request.url.startsWith('/Partidos_goles_equipo/')) {
            let respuesta = {};
            let tokenRecibido = request.headers["authorization"];
            let token = Usuario.validarToken(tokenRecibido);
            if(tokenRecibido && token) {
                let nuevoToken = Usuario.generarToken(token.login, token.acceso);
                let parte = request.url.split('/');
                Partido.listarPartidosSeleccionGoles(token.login, token.acceso, parte[3], +parte[2]).then(resp =>{
                    resp.forEach(ele =>{
                        ele.SOLICITANTE = token.login;
                        ele.ACCESO = token.acceso;
                    })
                    respuesta.token = nuevoToken;
                    respuesta.error = false;
                    respuesta.mensajeError = '';
                    respuesta.partido = resp;
                    response.writeHead(200, { "Content-Type": "application/json" });
                    response.end(JSON.stringify(respuesta));
                }).catch(error =>{
                    response.writeHead(400, { "Content-Type": "application/json" });
                    respuesta.error = true;
                    respuesta.mensajeError = 'Error al procesar la petición o no se han seleccionado partidos: ' + error;
                    response.end(JSON.stringify(respuesta));
                })
            } else {
                response.writeHead(401, { "Content-Type": "application/json" });
                respuesta.error = true;
                respuesta.mensajeError = 'Usuario no autorizado ';
                response.end(JSON.stringify(respuesta));
            }
        } else if(request.url.startsWith('/Partido/')) {
            let respuesta = {};
            let tokenRecibido = request.headers["authorization"];
            let token = Usuario.validarToken(tokenRecibido);
            if(tokenRecibido && token) {
                let nuevoToken = Usuario.generarToken(token.login, token.acceso);
                let parte = request.url.split('/');
                Partido.buscarPartidoPorId(+parte[3], token.login, token.acceso).then(resp =>{
                    respuesta.token = nuevoToken;
                    respuesta.error = false;
                    respuesta.mensajeError = '';
                    respuesta.partido = resp;
                    response.writeHead(200, { "Content-Type": "application/json" });
                    response.end(JSON.stringify(respuesta));
                }).catch(error =>{
                    response.writeHead(400, { "Content-Type": "application/json" });
                    respuesta.error = true;
                    respuesta.mensajeError = 'Error al buscar el partido o no se han seleccionado partidos: ' + error;
                    response.end(JSON.stringify(respuesta))
                })
            } else {
                response.writeHead(401, { "Content-Type": "application/json" });
                respuesta.error = true;
                respuesta.mensajeError = 'Usuario no autorizado ';
                response.end(JSON.stringify(respuesta));
            }
        } else {
            let respuesta = {};
            response.writeHead(401, { "Content-Type": "application/json" });
            respuesta.error = true;
            respuesta.mensajeError = 'Servicio GET no reconocido';
            response.end(JSON.stringify(respuesta));
        }
    } else if(request.method === 'POST') {
        if(request.url === '/Partido') {
            let tokenRecibido = request.headers["authorization"];
            let token = Usuario.validarToken(tokenRecibido);
            if(tokenRecibido && token) {
                let nuevoToken = Usuario.generarToken(token.login, token.acceso);
                let respuesta = {};
                let body = [];
                request
                    .on('data', chunk =>{
                        body.push(chunk);
                    })
                    .on('end', () =>{
                        body = Buffer.concat(body).toString();
                        let partido = new Partido(JSON.parse(body));
                        partido.SOLICITANTE = token.login;
                        partido.ACCESO = token.acceso;
                        partido.crear().then(resp =>{
                            respuesta.token = nuevoToken;
                            respuesta.error = false;
                            respuesta.mensajeError = '';
                            respuesta.insertId = resp.insertId;
                            response.writeHead(200, { "Content-Type": "application/json" });
                            response.end(JSON.stringify(respuesta));
                        }).catch(error =>{
                            response.writeHead(400, { "Content-Type": "application/json" });
                            respuesta.error = true;
                            respuesta.mensajeError = 'Error al insertar: ' + error;
                            response.end(JSON.stringify(respuesta))
                        });
                    })
            }
            
        } else if(request.url === '/login'){
            let respuesta = {};
            let body = [];
            request
                .on('data', chunk =>{
                    body.push(chunk);
                })
                .on('end', () =>{
                    body = Buffer.concat(body).toString();
                    Usuario.validarUsuario(JSON.parse(body)).then(tokenGenerado =>{
                        respuesta.error = false;
                        respuesta.token = tokenGenerado;
                        respuesta.mensajeError = '';
                        response.writeHead(200, { "Content-Type": "application/json" });
                        response.end(JSON.stringify(respuesta));
                    }).catch(error =>{
                        respuesta.error = true;
                        respuesta.mensajeError = 'Error al autenticar: ' + error;
                        response.writeHead(401, { "Content-Type": "application/json" });
                        response.end(JSON.stringify(respuesta));
                    })
                })
        } else {
            let respuesta = {};
            response.writeHead(401, { "Content-Type": "application/json" });
            respuesta.error = true;
            respuesta.mensajeError = 'Servicio POST no reconocido';
            response.end(JSON.stringify(respuesta));
        }
    } else if(request.method === 'PUT') {
        if(request.url === '/Partido') {
            let tokenRecibido = request.headers["authorization"];
            let token = Usuario.validarToken(tokenRecibido);
            if(tokenRecibido && token) {
                let nuevoToken = Usuario.generarToken(token.login, token.acceso);
                let respuesta = {};
                let body = [];
                request
                    .on('data', chunk =>{
                        body.push(chunk);
                    })
                    .on('end', () =>{
                        body = Buffer.concat(body).toString();
                        let partido = new Partido(JSON.parse(body));
                        partido.SOLICITANTE = token.login;
                        partido.ACCESO = token.acceso;
                        partido.actualizar().then(resp=>{
                            respuesta.token = nuevoToken;
                            respuesta.error = false;
                            respuesta.mensajeError = '';
                            respuesta.affectedRows = resp.affectedRows;
                            response.writeHead(200, { "Content-Type": "application/json" });
                            response.end(JSON.stringify(respuesta));
                        }).catch(error =>{
                            response.writeHead(400, { "Content-Type": "application/json" });
                            respuesta.error = true;
                            respuesta.mensajeError = 'Error al insertar: ' + error;
                            response.end(JSON.stringify(respuesta))
                        });
                    })
            }
        } else {
            let respuesta = {};
            response.writeHead(401, { "Content-Type": "application/json" });
            respuesta.error = true;
            respuesta.mensajeError = 'Servicio PUT no reconocido';
            response.end(JSON.stringify(respuesta));
        }
    } else if(request.method === 'DELETE') {
        if(request.url.startsWith('/Partido/')) {
            let respuesta = {};
            let tokenRecibido = request.headers["authorization"];
            let token = Usuario.validarToken(tokenRecibido);
            if(tokenRecibido && token) {
                let nuevoToken = Usuario.generarToken(token.login, token.acceso);
                let parte = request.url.split('/');
                Partido.buscarPartidoPorId(+parte[2], token.login, token.acceso).then(resp =>{
                    resp.borrar().then(res =>{
                        respuesta.token = nuevoToken;
                        respuesta.error = false;
                        respuesta.mensajeError = '';
                        respuesta.affectedRows = res.affectedRows;
                        response.writeHead(200, { "Content-Type": "application/json" });
                        response.end(JSON.stringify(respuesta));
                    }).catch(err =>{
                        response.writeHead(400, { "Content-Type": "application/json" });
                        respuesta.error = true;
                        respuesta.mensajeError = 'Error al eliminar el partido o no se han seleccionado partidos: ' + error;
                        response.end(JSON.stringify(respuesta));
                    })
                }).catch(error =>{
                    response.writeHead(400, { "Content-Type": "application/json" });
                    respuesta.error = true;
                    respuesta.mensajeError = 'Error ese partido no existe en la BBDD: ' + error;
                    response.end(JSON.stringify(respuesta));
                })
            } else {
                response.writeHead(401, { "Content-Type": "application/json" });
                respuesta.error = true;
                respuesta.mensajeError = 'Usuario no autorizado ';
                response.end(JSON.stringify(respuesta));
            }
        } else {
            let respuesta = {};
            response.writeHead(401, { "Content-Type": "application/json" });
            respuesta.error = true;
            respuesta.mensajeError = 'Servicio DELETE no reconocido';
            response.end(JSON.stringify(respuesta));
        }
    } else {
        let respuesta = {};
        response.writeHead(404, { "Content-Type": "application/json" });
        respuesta.error = true;
        respuesta.mensajeError = 'Servicio no reconocido';
        response.end(JSON.stringify(respuesta));
    }
}

http.createServer(atenderPeticion).listen(8888);